# MALIS

This repository contains the code that was used for our project : "ML Techniques for Brain Tumor Detection (Brain Tumor Detection Using SVM And CNN On MRI Images)" <br/><br/>
Members : <br/>
CORDINA Emmanuel <br/>
HUILLET Thibault <br/>
RADJOU Aditya

The main files are : svm.ipynb and cnn.ipynb. <br/>
In the file svm.ipynb, we provide the code we used to train SVM models. <br/>
In the file cnn.ipynb, we provide the code we used to train NN models and CNN models. <br/> <br/>
In the file 3D-cnn-model-with-axis-normalization.ipynb, we provide a code we found that train 3D-CNN models. <br/>
In the file Notebook_EDA.ipynb, we provide a code we used to explore the data of the 3D-CNN dataset competition.

Finally, please refer to our final deliverable to see our comments and which model is the best in our case.
